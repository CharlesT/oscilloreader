/**********************************************
 GLOBAL CONSTANTS
 Unchanging, and define the app's behaviour.
**********************************************/

const numSamples = 1024;
const waveType = 'sine';
const numDisplays = 4;

const fMax = 440;
const fMin = 1;
const aMax = 0.5;
const aMin = -0.5;

let initialFreq = 376;
let initialAmp = 0.5;

/**********************************************
 Code for generating an oscilloscope display.
**********************************************/

const o_display = (osc) => (controlsDiv) => (s) => {

    // Analyses data for printing
    const analyzer = new p5.FFT(0, numSamples);
    // Sample data source
    let freq = initialFreq;
    let amp = initialAmp;

    /**
     Setup code. Initialises the canvas and page elements relevant
     to the display.
    **/
    s.setup = () => {
        s.createCanvas(numSamples, fMax);
        s.noFill();
        s.stroke(240);

        osc.freq(freq, 0.01);
        osc.amp(amp, 0.01);
        osc.start();

        analyzer.setInput(osc);

        // Make sliders for frequency and amplitude
        const fSlider = makeSlider(
            fMin, fMax, initialFreq, 1, "freq", "f");
        const aSlider = makeSlider(
            aMin, aMax, initialAmp, 0.05, "amp", "a");

        // Add the sliders to the div.
        const freqMessage = "Adjust Frequency: ";
        const ampMessage = "Adjust Amplitude: ";
        const freqText = makeText(freqMessage + initialFreq);
        const ampText = makeText(ampMessage + initialAmp);

        controlsDiv.appendChild(freqText);
        controlsDiv.appendChild(fSlider);
        controlsDiv.appendChild(ampText);
        controlsDiv.appendChild(aSlider);

        fSlider.oninput = () => {
            freq = parseInt(fSlider.value, 10);
            osc.freq(freq, 0.01);
            freqText.innerHTML = freqMessage + freq;
        };

        aSlider.oninput = () => {
            amp = parseFloat(aSlider.value);
            osc.amp(amp, 0.01);
            ampText.innerHTML = ampMessage + amp;
        };
    };

    /**
     Drawing code. Runs every frame, so anything persistent that will
     be on the canvas must go in here.
    **/
    s.draw = () => {
        s.background(0);

        const samples = analyzer.waveform();
        const bufLen = samples.length;

        // Draw the oscillation
        s.strokeWeight(4);
        s.beginShape();
        for (var i = 0; i < bufLen; i++) {
            const x = s.map(i, 0, bufLen, 0, s.width);
            const y = s.map(samples[i], -1, 1, -s.height / 2, s.height / 2);
            s.vertex(x, y + s.height / 2);
        }
        s.endShape();

        // Add labels for the user to understand.
        labelStuff(s)(freq)(amp);
    };
};

// Add information about the current data to the canvas.
const labelStuff = (sketch) => (freq) => (amp) => {
    sketch.strokeWeight(1);
    sketch.text('Source: Sine wave', 20, 30);
    // sketch.text('Frequency: ' + freq, 20, 40);
    // sketch.text('Amplitude: ' + amp, 20, 60);
};

/**********************************************
 Initialisation code, run after the page has 
 loaded. Necessary because the body must be
 modifiable in order to add elements.
**********************************************/

window.onload = () => {
    // Dynamically make some displays
    for (let i = 1; i <= numDisplays; i++) {
        // Create the outer and inner div.
        const innerDiv = document.createElement('div');
        innerDiv.className = "display";
        innerDiv.id = "inner" + String(i);

        const controls = document.createElement('div');
        controls.className = "controls";


        const newCard = makeCard("card" + i, innerDiv);
        document.body.appendChild(newCard);

        // Example data source (to be replaced)
        const osc = new p5.Oscillator(10, waveType);

        // Add the canvas to the div.
        const newdisplay = new p5(
            o_display(osc)(controls), innerDiv);

        // Add the controls
        innerDiv.appendChild(controls);
    };
};

// Make a range-based slide, can have floating point vals if necessary.
const makeSlider = (min, max, defaultVal, step, _class, id) => {
    const slider = document.createElement('input');
    slider.type = 'range';
    slider.className = _class;
    slider.min = min;
    slider.max = max;
    slider.value = String(defaultVal);
    slider.step = step;
    slider.idName = id;
    return slider;
};

const makeCard = (id, innerDiv) => {
    const card = document.createElement('div');
    card.className = "card";
    const container = document.createElement('div');
    card.appendChild(innerDiv);
    card.appendChild(container);
    return card;
};

const makeText = (text) => {
    const textDiv = document.createElement('div');
    textDiv.className = "text";
    textDiv.innerHTML = text;
    return textDiv;
};
