import asyncio
import websockets
import time
from datetime import datetime

VALS_PER_SEC = 4000
VALS_PER_PAC = 100
COMM_INTERVAL = VALS_PER_PAC / VALS_PER_SEC

def give(filename):
  async def innerF(websocket, path):
    vals = []
    with open(filename, 'r') as f:
      await websocket.send(str(VALS_PER_SEC))
      for line in f:
        vals.append(line.strip())
    while True:
      packet = []
      for v in vals:
        packet.append(str(v))
        if len(packet) == VALS_PER_PAC:
          time.sleep(COMM_INTERVAL)
          await websocket.send(','.join(packet))
          packet = []

  return innerF


start_server = websockets.serve(give("./data/Z/Z001.txt"), "127.0.0.1", 65433)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
