from math import sin, tan, pi

with open("sin.txt", "w+") as f:
  a = 0
  for _ in range(0,1000000):
    f.write("{},".format(sin(a)))
    a += pi / 12

with open("square.txt", "w+") as f:
  a = 0
  for _ in range(0,1000000):
    if a % 4 < 2:
      f.write("{},".format(1))
    else:
      f.write("{},".format(-1))

    a += 0.2


with open("tan.txt", "w+") as f:
  a = 0
  for _ in range(0,1000000):
    f.write("{},".format(tan(a)))
    a += pi / 25
