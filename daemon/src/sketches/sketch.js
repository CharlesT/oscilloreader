/**********************************************************
 A P5.js sketch intended for use with React.js, which mimics
 the display of a basic oscilloscope. 

 Data is retrieved from a websocket connection; in order to 
 function, a separate program must be running on the network
 and sending packets of comma separated values every second.

 @param {Object} p - contains all p5-related functions and values.
**********************************************************/
export default function sketch(p) {

    /*
      CONSTANTS
    */
    const v_grid_regions = 20;
    const h_grid_regions = 10;

    /*
      GLOBAL CONFIGURATION VARIABLES
    */
    let units = "";
    let upd = 1;
    let mpd = 50;
    let vdisp = 0;
    let hdisp = 0;
    let sLevel = 0;
    let slopeIsPos = true;
    // These change when the websocket connection is established.
    let packetSize = 3000;
    let framerate = 25;
    let isTriggerEnabled = true;

    /*
      GLOBAL VARIABLES
    */
    let ws;
    let dataBuffer = [];
    let currData = Array(1).fill(0);


    /**
       Initialise the canvas and set basic parameters
    */
    p.setup = () => {
        p.createCanvas(1200, 600);
        p.frameRate(framerate);
        p.noFill();
        p.stroke(240);
    };

    /**
       Update parameters when React calls for a re-render.
       @param {Object} props - contains all configuration variables provided by react.js
    */
    p.myCustomRedrawAccordingToNewPropsHandler = (props) => {

        // Update basic parameters
        upd = props.upd;
        mpd = props.mpd;
        vdisp = parseFloat(props.vdisp);
        hdisp = parseFloat(props.hdisp);
        sLevel = props.sLevel;
        slopeIsPos = props.slopeIsPos;
        isTriggerEnabled = props.triggerOn;
        framerate = props.fps;

        units = props.units;


        // Setup the websocket if it hasn't been already, according
        // to the IP and port provided by react.
        if (ws === undefined) {
            ws = new WebSocket("ws://" + props.ip + ':' + props.port);

            ws.onopen = () => {
                ws.send("No message");
                // alert("Message is sent...");
            };

            ws.onmessage = (evt) => {
                const newData = evt.data.split(',');
                // Collect initialisation data.
                if (newData.length === 1) {
                    packetSize = newData[0];
                }
                else {
                    dataBuffer = dataBuffer.concat(newData);
                }
                // alert("Message is received");
            };

            ws.onclose = function() {
                // alert("Connection is closed");
            };
        }
    };

    /**
       Code for drawing the oscilloscope display - runs every frame. 
       Note that the FPS will vary, but the data should still be represented
       properly due to use of p.deltaTime.
    */
    p.draw = () => {
        p.background(100);

        // Use MPD data to adjust data storage and determine how much data to
        // display in this frame.
        const timePerFrame = 1 / framerate;
        const sectionSize = Math.floor(timePerFrame * packetSize);

        console.log(units, dataBuffer.length);

        // Only update display data if there's enough to display and it's timely.
        // Note that there should always be a little extra, to account for the tail end.
        while (dataBuffer.length > 1.5 * sectionSize) {

            // MANAGE THE SCOPE TRIGGER
            if (isTriggerEnabled) {
                const level = (-sLevel - vdisp) * upd; // Dunno why but this has to be negated.

                const firstTrig = getTriggerPos(level, slopeIsPos, dataBuffer, 0);
                const secondTrig = getTriggerPos(level, slopeIsPos, dataBuffer, sectionSize);
                const finalTrig = secondTrig === null ? sectionSize : secondTrig;

                // secondTrig is allowed to be null, but if the first is null then there's a
                // problem. Most likely the trigger value the user selected is bad.
                if (firstTrig === null) {
                    currData = [];
                    dataBuffer = dataBuffer.slice(finalTrig);
                } else {
                    currData = dataBuffer.slice(firstTrig, finalTrig);
                    dataBuffer = dataBuffer.slice(finalTrig);
                }
            } else {
                currData = dataBuffer.slice(0, sectionSize);
                dataBuffer = dataBuffer.slice(sectionSize);
            }
        }


        // Draw horizontal line
        p.strokeWeight(1);
        p.line(0, p.height / 2, p.width, p.height / 2);

        const divs = gridLines(v_grid_regions, h_grid_regions);
        const unit_width = divs[0];
        const unit_height = divs[1];
        const totalWidth = (packetSize / framerate / v_grid_regions / mpd) * p.width;

        // Draw the oscillation
        p.strokeWeight(4);
        drawWaveform(totalWidth, currData, unit_height, unit_width);


        // Draw Updates per second (not FPS); the number of time sthe data is updated 
        // per second.
        p.strokeWeight(1);
        // Add a descriptor of the units in use:
        const unitText = 'Units: ' + units;
        p.text(unitText, 5, 15);
    };

    /**
       Draw a waveform of a specified length, sourced from an array. Adjust it according
       to provided and global parameters.
       @param {number} graph_width - x-value to spread the graph to.
       @param {number[]} values - array of oscillating values to graph.
       @param {number} unit_height - height of a single 'unit' (specified by source)
       @param {number} unit_width - likewise, the 'unit' width.
    */
    const drawWaveform = (graph_width, values, unit_height, unit_width) => {
        p.beginShape();
        for (let i = 0; i < values.length; i++) {
            const initX = i;
            const initY = parseFloat(values[i]);

            const x = p.map(initX, 0, values.length, 0, graph_width) + hdisp * unit_width;
            const y = ((initY / upd + vdisp) * unit_height) + p.height / 2;

            p.vertex(x, y);
        }
        p.endShape();
    };

    /**
       Find the first point in allData which has the same value as 'level', and with
       the same gradient.
       @param {number} level - value to search for.
       @param {boolean} slope - gradient to satisfy.
       @param {number[]} allData - values to look through.
       @param {number} startPos - position in allData at which to start the search.
       @returns {number} first valid trigger value.
    */
    const getTriggerPos = (level, slope, allData, startPos) => {

        let prev = parseFloat(allData[0]);
        let oldDiff = prev - level;
        const length = allData.length;

        for (let i = startPos; i < length; i++) {
            const initY = parseFloat(allData[i]);
            const newDiff = initY - level;
            const currSlopeIsPos = initY - prev >= 0;

            const posGradMatch = oldDiff <= 0 && newDiff >= 0;
            const negGradMatch = oldDiff >= 0 && newDiff <= 0;

            if (posGradMatch || negGradMatch) {
                if (currSlopeIsPos !== slope) {
                    return i;
                }
            }
            oldDiff = newDiff;
            prev = initY;
        }

        console.log("Here");
        return null;
    };

    /* Truncate a value to 'places' decimal places */
    const truncate = (x, places) => Math.floor(x * (10 ** places)) / (10 ** places);

    /*
      Draw 'vert_no' of evenly spaced vertical grid-lines, and likewise horizontal lines
      according to horz_no.
    */
    function gridLines(vert_no, horz_no) {
        p.strokeWeight(0.3);
        const width_div = p.width / vert_no;
        const height_div = p.height / horz_no;

        const v = vert_no;
        const h = horz_no;

        for (let i = 1; i < v; i++) {
            p.line(i * width_div, 0, i * width_div, p.height);
        }

        const mid = p.height / 2;
        for (let i = 1; i < h / 2; i++) {
            p.line(0, mid - i * height_div, p.width, mid - i * height_div);
            p.line(0, mid + i * height_div, p.width, mid + i * height_div);
        }

        return [width_div, height_div];
    }

    // function dottedLineV(x, width, spacing) {
    //     const period = width + spacing;
    //     for (let i = 0; i <= p.height - period; i += period) {
    //         p.line(x, i, x, i + width);
    //     }
    // }

    // function dottedLineH(y, width, spacing) {
    //     const period = width + spacing;
    //     for (let i = 0; i <= p.width - period; i += period) {
    //         p.line(i, y, i + width, y);
    //     }
    // }
};
