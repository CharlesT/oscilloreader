import React from 'react';
import './App.css';

// Display managed by P5.js
import P5Wrapper from 'react-p5-wrapper';
import sketch from './sketches/sketch';

// BOOTSTRAP
import 'bootstrap/dist/css/bootstrap.min.css';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
// import Nav from 'react-bootstrap/Nav';
import CardDeck from 'react-bootstrap/CardDeck';
import Container from 'react-bootstrap/Container';


const compose = (f, g) => (x) => f(g(x));
const truncate = (x, t) => x.toFixed(t);


/**********************************************************
 Parent component, contains all other components. 
**********************************************************/
class App extends React.Component {
    /*
      Initialise the displays and state.
      Plan to read config file here.
    */
    constructor(props) {
        super(props);

        const displays = [];
        // for (let i = 1; i <= 3; i++)
        //     displays.push(this.newCardState(i, i, 65432 + i, 50));
        displays.push(this.newCardState("Sine wave", displays.length, 65433, 2.5, 0.5, 20, 0, "N/A", true, 0));
        displays.push(this.newCardState("ECG Noise", displays.length, 65434, 2.5, 2500, 20, -6, "mV", false, 0));
        displays.push(this.newCardState("ECG Recordings of a subject being tested for arrhythmia", displays.length, 65435, 4, 1, 25, 0, "mV", true, -0.1));
        // displays.push(this.newCardState("Tan wave", displays.length, 65434, 8));
        // displays.push(this.newCardState("Square wave", displays.length, 65435, 8));

        this.state = {
            displays: displays,
        };
    }

    /*
      Generate new state for a display (card), using default values where
      appropriate.
    */
    newCardState = (title, i, port, mpd, upd, fps, vdisp, units, trigOn, trig) => {
        return {
            title: title,
            units: units,
            index: i,
            vdisp: vdisp,
            hdisp: 0,
            mpd: mpd,
            upd: upd,
            fps: fps,
            sLevel: trig,
            slopeIsPos: true,
            triggerOn: trigOn,
            ip: "localhost",
            port: port,
        };
    }


    /*
      Change the attributes of display at index 'i' in the global state,
      according to the 'modifier' state constructor and the event value.
    */
    modDisplay = (i) => (modifier) => (name) => (x) => () => {
        const displays = this.state.displays.slice();
        displays[i] = { ...displays[i], ...modifier(x) };
        this.setState({ displays: displays });
    }

    /*
      State modifiers for each type of number input. 
      TODO: Possible refactor into a more re-usable function.
    */
    vdChange = (full) => this.modDisplay(full.index)(x => ({ vdisp: x }))("vdisp");
    hdChange = (full) => this.modDisplay(full.index)(x => ({ hdisp: x }))("hdisp");
    mpdChange = (full) => this.modDisplay(full.index)(x => ({ mpd: x }))("mpd");
    updChange = (full) => this.modDisplay(full.index)(x => ({ upd: x }))("upd");
    sLevelChange = (full) => this.modDisplay(full.index)(x => ({ sLevel: x }))("sLevel");
    fpsChange = (full) => this.modDisplay(full.index)(x => ({ fps: x }))("fps");

    sSlopeChange = (full) => (x) => (p) => () => {
        const i = full.index;
        const displays = this.state.displays.slice();
        displays[i] = { ...displays[i], slopeIsPos: x };
        this.setState({ displays: displays });
    }

    triggerChange = (full) => (x) => (p) => () => {
        const i = full.index;
        const displays = this.state.displays.slice();
        displays[i] = { ...displays[i], triggerOn: x };
        this.setState({ displays: displays });
    }

    /*
      Take a valid card/display state, and use it to construct a CardM 
      component.
    */
    newCard = (dispState) => {
        return (
            <CardM title={dispState.title}>
                <P5Wrapper
                    sketch={sketch}
                    units={dispState.units}
                    width='300'
                    height='300'
                    vdisp={dispState.vdisp}
                    hdisp={dispState.hdisp}
                    mpd={dispState.mpd}
                    upd={dispState.upd}
                    fps={dispState.fps}
                    sLevel={dispState.sLevel}
                    slopeIsPos={dispState.slopeIsPos}
                    triggerOn={dispState.triggerOn}
                    ip={dispState.ip}
                    port={dispState.port}
                />
                <Inputs
                    vals={dispState}
                    vdChange={this.vdChange(dispState)}
                    hdChange={this.hdChange(dispState)}
                    mpdChange={this.mpdChange(dispState)}
                    updChange={this.updChange(dispState)}
                    fpsChange={this.fpsChange(dispState)}
                    sLevelChange={this.sLevelChange(dispState)}
                    sSlopeChange={this.sSlopeChange(dispState)}
                    triggerChange={this.triggerChange(dispState)}
                />
            </CardM>
        );
    }

    /*
      Render all cards currently in state. 
    */
    render() {
        return (
            <Container>
                <Row>
                    <Col lg='12'>
                        {this.state.displays.map((dispState) => (
                            this.newCard(dispState)
                        ))}
                    </Col>
                </Row>
            </Container>
        );
    }
}

/**********************************************************
 Simple wrapper for bootstrap's 'Card' component. Sets the title
 and adds a div for formatting purposes.
**********************************************************/
const CardM = (props) => (
    <div>
        <Card>
            <Card.Header as='h5'>{props.title}</Card.Header>
            <Card.Body>
                <Card.Text>
                    {props.children}
                </Card.Text>
            </Card.Body>
        </Card>
    </div>
);


/**********************************************************
 A number spinner component, intended to be added to a 
 Bootstrap form. It has a label to its left.
**********************************************************/
const Spinner = (props) => (
    <Form.Group as={Row} controlId="freq">
        <Form.Label column sm="6">
            {props.name}
        </Form.Label>
        <Col sm="6">
            <Row>
                <Button variant="outline-secondary"
                    active={true}
                    onClick={props.onInput(truncate(+props.value - +props.buttonStep, 2))}
                >-</Button>
                <Form.Control
                    type="number"
                    value={props.value}
                    onChange={(e) => props.onInput(e.target.value)()}
                    max={props.max}
                    min={props.min}
                    step={props.step}
                />
                <Button variant="outline-secondary"
                    active={true}
                    onClick={props.onInput(truncate(+props.value + +props.buttonStep, 2))}
                >+</Button>
            </Row>
        </Col>
    </Form.Group>
);

const ThisOrThat = (props) => (
    <Form.Group as={Row} controlId="freq">
        <Form.Label column sm="6">
            {props.name}
        </Form.Label>
        <Col sm="6">
            <Button variant="outline-secondary"
                onClick={props.doThat(props)}
                active={!props.val} >{props.labelThat}</Button>
            <Button variant="outline-secondary"
                onClick={props.doThis(props)}
                active={props.val}>{props.labelThis}</Button>
        </Col>
    </Form.Group>
);


/**********************************************************
 Uses the above Spinner component to create a grid of the 
 inputs necessary for a CardM component.
**********************************************************/
const Inputs = (props) => (
    <Container>
        <Row>
            <Col>
                <Spinner
                    name='Milliseconds per horizontal div'
                    value={props.vals.mpd}
                    onInput={props.mpdChange}
                    max='10000'
                    min='0.01'
                    step='0.01'
                    buttonStep='1'
                />
            </Col>
            <Col>
                <Spinner
                    name={"Units (" + props.vals.units + ') per vertical div'}
                    value={props.vals.upd}
                    onInput={props.updChange}
                    max='100000000'
                    min='0.1'
                    step='0.1'
                    buttonStep='0.5'
                />
            </Col>
        </Row>
        <Row>
            <Col>
                <Spinner
                    name='Vertical position'
                    value={props.vals.vdisp}
                    onInput={props.vdChange}
                    max='10000'
                    min='-10000'
                    step='0.1'
                    buttonStep='1'
                />
            </Col>
            <Col>
                <Spinner
                    name='Horizontal position'
                    value={props.vals.hdisp}
                    onInput={props.hdChange}
                    max='10000'
                    min='-10000'
                    step='0.1'
                    buttonStep='1'
                />
            </Col>
        </Row>
        <Row>
            <Col>
                <Spinner
                    name='Trigger level (in divs)'
                    value={props.vals.sLevel}
                    onInput={props.sLevelChange}
                    max='10000'
                    min='-10000'
                    step='0.01'
                    buttonStep='0.1'
                />
            </Col>
            <Col>
                <ThisOrThat
                    name='Trigger slope'
                    val={props.vals.slopeIsPos}
                    doThis={props.sSlopeChange(true)}
                    doThat={props.sSlopeChange(false)}
                    labelThis='+'
                    labelThat='-'
                />
            </Col>
        </Row>
        <Row>
            <Col>
                <Spinner
                    name='Samples per second'
                    value={props.vals.fps}
                    onInput={props.fpsChange}
                    max='100'
                    min='1'
                    step='1'
                    buttonStep='1'
                />
            </Col>
            <Col>
                <ThisOrThat
                    name='Trigger switch'
                    val={props.vals.triggerOn}
                    doThis={props.triggerChange(true)}
                    doThat={props.triggerChange(false)}
                    labelThis="On"
                    labelThat="Off"
                />
            </Col>
        </Row>
    </Container >
);

export default App;
