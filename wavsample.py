import asyncio
import websockets
import time
from scipy.io import wavfile
from datetime import datetime

VALS_PER_SEC = 1000
VALS_PER_PAC = 50
COMM_INTERVAL = VALS_PER_PAC / VALS_PER_SEC

_, vals = wavfile.read("data/ecgwav_noise.wav")

def give(filename):
  async def innerF(websocket, path):
    while True:
      await websocket.send(str(VALS_PER_SEC))
      packet = []
      for v in vals:
        packet.append(str(v))
        if len(packet) == VALS_PER_PAC:
          time.sleep(COMM_INTERVAL)
          await websocket.send(','.join(packet))
          packet = []

  return innerF


start_server = websockets.serve(give("ecgwav_noise.wav"), "127.0.0.1", 65434)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
