/**********************************************
 GLOBAL CONSTANTS
 Unchanging, and define the app's behaviour.
**********************************************/

const numSamples = 1024;
const waveType = 'sine';
const numDisplays = 1;

const dispMax = 100;
const dispMin = -100;
const t_scaleMax = 10;
const t_scaleMin = 0.01;
const a_scaleMax = 10;
const a_scaleMin = 0.1;

const initialDisp = 0;
const initialTimeScale = 1;
const initialAmpScale = 1;

/**********************************************
 Code for generating an oscilloscope display.
**********************************************/

const o_display = (osc) => (controlsDiv) => (s) => {

    // Analyses data for printing
    const analyzer = new p5.FFT(0, numSamples);

    // configurable values
    let time_displacement = 0;
    let time_scaling = 1;
    let amplitude_scaling = 1;

    /**
       Setup code. Initialises the canvas and page elements relevant
       to the display.
    **/
    s.setup = () => {
        s.createCanvas(numSamples, fMax);
        s.noFill();
        s.stroke(240);

        osc.freq(freq, 0.01);
        osc.amp(amp, 0.01);
        osc.start();

        analyzer.setInput(osc);



        // Make sliders for frequency and amplitude
        const dispSlider = makeSlider(
            dispMin, dispMax, initialDisp, dispMin, "time_disp", "t");
        const t_scaleSlider = makeSlider(
            t_scaleMin, t_scaleMax, initialTimeScale, t_scaleMin, "t_scale", "ts");
        const a_scaleSlider = makeSlider(
            a_scaleMin, a_scaleMax, initialAmpScale, a_scaleMin, "a_scale", "as");

        // Add the sliders to the div.
        const dispMessage = "Adjust Temporal Position: ";
        const t_scaleMessage = "Adjust Temporal Zoom: ";
        const a_scaleMessage = "Adjust Amplitude Scaling: ";
        const dispText = makeText(dispMessage + initialDisp);
        const t_scaleText = makeText(t_scaleMessage + initialTimeScale);
        const a_scaleText = makeText(a_scaleMessage + initialAmpScale);

        controlsDiv.appendChild(dispText);
        controlsDiv.appendChild(dispSlider);
        controlsDiv.appendChild(t_scaleMessage);
        controlsDiv.appendChild(t_scaleSlider);
        controlsDiv.appendChild(a_scaleMessage);
        controlsDiv.appendChild(a_scaleSlider);

        dispSlider.oninput = () => {
            time_displacement = parseFloat(dispSlider.value);
            dispText.innerHTML = dispMessage + time_displacement;
        };

        t_scaleSlider.oninput = () => {
            time_scaling = parseFloat(t_scaleSlider.value);
            t_scaleText.innerHTML = t_scaleMessage + time_scaling;
        };

        a_scaleSlider.oninput = () => {
            amplitude_scaling = parseFloat(a_scaleSlider.value);
            a_scaleText.innerHTML = a_scaleMessage + amplitude_scaling;
        };
    };

    /**
       Drawing code. Runs every frame, so anything persistent that will
       be on the canvas must go in here.
    **/
    s.draw = () => {
        s.background(0);

        const samples = analyzer.waveform();
        const bufLen = samples.length;

        // Draw the oscillation
        s.strokeWeight(4);
        s.beginShape();
        for (var i = 0; i < bufLen; i++) {
            const x = s.map(i, 0, bufLen, 0, s.width);
            const y = s.map(samples[i], -1, 1, -s.height / 2, s.height / 2);
            s.vertex(x, y + s.height / 2);
        }
        s.endShape();
    };
};

// Add information about the current data to the canvas.
const labelStuff = (sketch) => (freq) => (amp) => {
    sketch.strokeWeight(1);
    sketch.text('Source: Sine wave', 20, 30);
    // sketch.text('Frequency: ' + freq, 20, 40);
    // sketch.text('Amplitude: ' + amp, 20, 60);
};

/**********************************************
 Initialisation code, run after the page has 
 loaded. Necessary because the body must be
 modifiable in order to add elements.
**********************************************/

window.onload = () => {
    // Dynamically make some displays
    for (let i = 1; i <= numDisplays; i++) {
        // Create the outer and inner div.
        const innerDiv = document.createElement('div');
        innerDiv.className = "display";
        innerDiv.id = "inner" + String(i);

        const controls = document.createElement('div');
        controls.className = "controls";


        const newCard = makeCard("card" + i, innerDiv);
        document.body.appendChild(newCard);

        // Example data source (to be replaced)
        const osc = new p5.Oscillator(10, waveType);

        // Add the canvas to the div.
        const newdisplay = new p5(
            o_display(osc)(controls), innerDiv);

        // Add the controls
        innerDiv.appendChild(controls);
    };
};

// Make a range-based slide, can have floating point vals if necessary.
const makeSlider = (min, max, defaultVal, step, _class, id) => {
    const slider = document.createElement('input');
    slider.type = 'range';
    slider.className = _class;
    slider.min = min;
    slider.max = max;
    slider.value = String(defaultVal);
    slider.step = step;
    slider.idName = id;
    return slider;
};

const makeCard = (id, innerDiv) => {
    const card = document.createElement('div');
    card.className = "card";
    const container = document.createElement('div');
    card.appendChild(innerDiv);
    card.appendChild(container);
    return card;
};

const makeText = (text) => {
    const textDiv = document.createElement('div');
    textDiv.className = "text";
    textDiv.innerHTML = text;
    return textDiv;
};
